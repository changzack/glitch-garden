﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerResource : MonoBehaviour
{
    public static PlayerResource Instance { private set; get; }
    
    Text text;

    [SerializeField] int stars = 500;
    public int Stars {
        private set => stars = value;
        get => stars;
    }

    #region Unity Events

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else if (Instance == null)
        {
            Instance = this;
            text = GetComponent<Text>();
        }
    }

    void Start()
    {
        UpdateResourceDisplay();
    }

    #endregion

    #region Public Methods

    public void AddStars(int amount)
    {
        Stars += amount;
        UpdateResourceDisplay();
    }

    public void DecreaseStarts(int amount)
    {
        Stars -= amount;
        UpdateResourceDisplay();
    }

    public bool HasEnoughStars(int defenderCost)
    {
        return Stars >= defenderCost;
    }

    #endregion

    #region Private Methods

    void UpdateResourceDisplay()
    {
        text.text = Stars.ToString();
    }

    #endregion
}
