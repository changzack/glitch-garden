﻿using UnityEngine;

public class Damageable : MonoBehaviour
{
    [SerializeField] GameObject deathVfxPrefab;
    public float health = 100f;
        
    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            TriggerDeathVFX();
            Destroy(gameObject);
        }
    }
        
    void TriggerDeathVFX()
    {
        var deathVfx = Instantiate(deathVfxPrefab, transform.position, Quaternion.identity);
        Destroy(deathVfx, 1f);
    }
}