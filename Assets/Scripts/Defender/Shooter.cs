﻿using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] GameObject gun, projectile;

    /// <summary>
    /// Animation event
    /// </summary>
    public void Fire()
    {
        Instantiate(projectile, gun.transform.position, Quaternion.identity);
    }
}
