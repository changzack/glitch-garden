﻿using DG.Tweening;
using UnityEngine;

namespace DefaultNamespace
{
    public class Star : MonoBehaviour
    {
        [SerializeField] int starValue;
        SpriteRenderer spriteRenderer;
        
        void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Play() 
        {
            transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 1)
                     .SetEase(Ease.InQuint);
            
            transform.DOMove(PlayerResource.Instance.transform.position, 1)
                     .OnComplete(() =>
                     {
                         PlayerResource.Instance.AddStars(starValue);
                         Destroy(gameObject);
                     });
        }
    }
}