﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour
{
    #region Fields and Properties

    Defender selectedDefender;
    readonly ISet<Vector2> defenderPositions = new HashSet<Vector2>();
    
    public static DefenderSpawner Instance { get; private set; }

    #endregion
    

    #region Unity Event Functions

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void OnMouseDown()
    {
        SpawnDefender(GetSquareClicked());
    }

    #endregion

    #region Public Methods

    public void SetSelectedDefender(Defender defender)
    {
        selectedDefender = defender;
    }

    #endregion

    #region Private Methods

    Vector2 GetSquareClicked()
    {
        Vector2 clickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(clickPos);
        Vector2 squarePos = new Vector2(Mathf.RoundToInt(worldPos.x), Mathf.RoundToInt(worldPos.y));
        return squarePos;
    }

    void SpawnDefender(Vector2 placePos)
    {
        if (PassSpawnConditions(placePos) == false)
        {
            return;
        } 

        PlaceDefender(placePos, out var newDefender);
    }

    bool PassSpawnConditions(Vector2 placePos)
    {
        if (selectedDefender == null)
        {
            return false;
        }

        if (HasEnoughResource(selectedDefender.cost) == false)
        {
            return false;
        }

        // Do not place if position already contains a defender
        if (defenderPositions.Contains(placePos))
        {
            return false;
        }

        return true;
    }

    void PlaceDefender(Vector2 placePos, out Defender newDefender)
    {
        defenderPositions.Add(placePos);
        PlayerResource.Instance.DecreaseStarts(selectedDefender.cost);
        newDefender = Instantiate(selectedDefender, placePos, Quaternion.identity);
    }

    bool HasEnoughResource(int defenderCost)
    {
        if (PlayerResource.Instance.HasEnoughStars(defenderCost) == false)
        {
            Debug.Log($"Not enough stars to spawn {selectedDefender.name}");
            return false;
        }

        return true;
    }

    #endregion
}
