﻿﻿using UnityEngine;

namespace DefaultNamespace
{
    public class ResourceDefender : Defender
    {
        [SerializeField] Star star;
        [SerializeField] Transform starSpawnPosition;
        
        /// <summary>
        /// Animation event
        /// </summary>
        public void AddStar()
        {
            Instantiate(star, starSpawnPosition.position, Quaternion.identity).Play();
        }
    }
}