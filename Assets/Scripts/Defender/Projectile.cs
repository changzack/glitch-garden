﻿using System;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speed = 1f;
    [SerializeField] float damage = 10f;
    [SerializeField] float rotationAdd = 10f;

    Rigidbody2D rb;
    
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        rb.velocity = Vector2.right * speed;
    }

    void Update()
    {
        var currentAngle = rb.rotation;
        rb.MoveRotation(currentAngle + rotationAdd);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        var damageable = other.collider.GetComponent<Damageable>();
        if (damageable != null)
        {
            damageable.TakeDamage(damage);
            Destroy(gameObject);
        }
    }
    
    
}
