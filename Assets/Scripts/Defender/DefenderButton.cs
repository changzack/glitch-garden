﻿using UnityEngine;

public class DefenderButton : MonoBehaviour
{
    [SerializeField] Defender defenderPrefab;
    SpriteRenderer touchedIcon;
    static SpriteRenderer previousIcon;

    void Awake()
    {
        touchedIcon = GetComponent<SpriteRenderer>();
    }

    void OnMouseDown()
    {
        if (previousIcon != null)
        {
            previousIcon.color = Color.black;
        }
        
        touchedIcon.color = Color.white;
        previousIcon = touchedIcon;
        DefenderSpawner.Instance.SetSelectedDefender(defenderPrefab);
    }
}
