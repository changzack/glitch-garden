﻿using System.Collections;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour
{
    [SerializeField] float minSpawnDelay = 1f;
    [SerializeField] float maxSpawnDelay = 5f;
    [SerializeField] Attacker attacker;

    bool isSpawn = true;
    
    IEnumerator Start()
    {
        while (isSpawn)
        {
            var seconds = Random.Range(minSpawnDelay, maxSpawnDelay);
            yield return new WaitForSeconds(seconds);
            Spawn();
        }
    }

    void Spawn()
    {
        Instantiate(attacker, transform.position, transform.rotation, transform);
    }

}
