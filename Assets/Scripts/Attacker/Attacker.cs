﻿using UnityEngine;

public class Attacker : MonoBehaviour
{
    [Range(0f, 5f)]
    [SerializeField] private float currentSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * (currentSpeed * Time.deltaTime));
    }

    // Animation use
    public void SetCurrentSpeed(float speed)
    {
        currentSpeed = speed;
    }
}